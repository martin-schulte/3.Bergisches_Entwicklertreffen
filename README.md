# 3. Bergisches Entwicklertreffen

[https://www.meetup.com/de-DE/bergisches-entwicklerforum/events/292580228/](https://www.meetup.com/de-DE/bergisches-entwicklerforum/events/292580228/)

## Referenzen

* [Offizielle bash Doku](https://www.gnu.org/software/bash/manual/)
* [Google Style Guide](https://google.github.io/styleguide/shellguide.html)
* [Greg Wooledge's BashGuide und BashFAQ](https://mywiki.wooledge.org/)
